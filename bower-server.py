#!/usr/bin/env python

import SimpleHTTPServer
import SocketServer
import os
import time

# sudo lsof -i:8888 

class server(SimpleHTTPServer.SimpleHTTPRequestHandler):
    
    def do_POST(self):
        if self.path=="/ping":
            self.send_response(200)
            return
        else:
            self.send_response(501)
        
    def do_GET(self):
        return SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)


#os.chdir("/home/dave/code/bower")

PORT = 8890
SocketServer.TCPServer.allow_reuse_address = True
httpd = SocketServer.TCPServer(("127.0.0.1", PORT), server)
httpd.serve_forever()

