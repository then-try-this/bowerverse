(function () {
    'use strict';
    
    let xrButton = document.getElementById('xr-button');
    let aboutText = document.getElementById('game-text');
    let xrSession = null;
    let xrRefSpace = null;
    let gl = null;

    function initXR() {
        if (navigator.xr) {
            navigator.xr.isSessionSupported('immersive-vr').then((supported) => {
				if (supported) {
					xrButton.addEventListener('click', onButtonClicked);
					aboutText.textContent = 'We have detected a VR device!';
					xrButton.textContent = 'Click here to enter bowerbirds in 3D';
					xrButton.disabled = false;
					xrButton.style.display = "block";
				} else {
					setupNoXR();
				}				
			});
        } else {
			setupNoXR();
		}
    }
	
	function setupNoXR() {
		xrButton.addEventListener('click', onButtonClickedNoXR);
		aboutText.innerHTML = 'We have not detected a VR device, but you can still explore bowerbirds in 3D in your browser.<br>Please turn your sound on, and use the cursor keys (or w,a,s & d) to look around.';
		xrButton.textContent = 'Click here to enter bowerbirds in 3D';
		xrButton.disabled = false;
		xrButton.style.display = "block";
	}
	
    function onButtonClickedNoXR() {
		ditto_run(["scm/bower.jscm"]);  
		document.getElementById('game-info').style.display = "none";
    }

    function onButtonClicked() {
        if (!xrSession) {
	    navigator.xr.requestSession('immersive-vr').then(onSessionStarted);
        } else {
	    xrSession.end();
        }
    }
    
    function onSessionStarted(session) {
        xrSession = session;
        xrButton.textContent = 'Exit VR';
        session.addEventListener('end', onSessionEnded);

	let canvas = document.createElement('canvas');
        gl = canvas.getContext('webgl', { xrCompatible: true });
        session.updateRenderState({ baseLayer: new XRWebGLLayer(session, gl) });
	// pass in VR gl context
	flx_init_with_gl(gl);
	// run game script
	ditto_run(["scm/bower.jscm"])  
	
        session.requestReferenceSpace('local').then((refSpace) => {
	    xrRefSpace = refSpace;
	    session.requestAnimationFrame(onXRFrame);
        });
    }

    function onSessionEnded(event) {
        xrSession = null;
        xrButton.textContent = 'Enter VR';
	gl = null;
    }

    function onXRFrame(time, frame) {
        let session = frame.session;
        session.requestAnimationFrame(onXRFrame);
        let pose = frame.getViewerPose(xrRefSpace);
        if (pose) {
	    let glLayer = session.renderState.baseLayer;
            gl.bindFramebuffer(gl.FRAMEBUFFER, glLayer.framebuffer);
	    renderer_clear(the_renderer);
	    do_load_update();
	    renderer_run_hook(the_renderer);   
	    for (let view of pose.views) {
		let viewport = glLayer.getViewport(view);
		renderer_render_outer(the_renderer,viewport.x, viewport.y,
				      viewport.width, viewport.height,
				      view.projectionMatrix, view.transform.inverse.matrix);
		screen_width=viewport.width;
		screen_height=viewport.height;
	    }
        }
    }
    
    ditto_init(["flx/scm/fluxus.jscm"]);
    
    initXR();
    
})();
